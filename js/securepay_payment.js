/**
 * Javascript for SecurePay Payment.
 */

(function ($, Drupal, window, document, undefined) {
    $(document).ready(function(){
        $('#edit-creditcard').keypress(function() {
          if ($('#edit-creditcard').val().substring(0,2) < 56 && $('#edit-creditcard').val().substring(0,2) > 50) {
            $('.securepay_payment_cards .card.master img').fadeIn(1000);
            $('.securepay_payment_cards .card.visa img').fadeOut(1000);
            $('.securepay_payment_cards .card.amex img').fadeOut(1000);
          }
          else if ($('#edit-creditcard').val().substring(0,1) == 4) {
            $('.securepay_payment_cards .card.visa img').fadeIn(1000);
            $('.securepay_payment_cards .card.master img').fadeOut(1000);
            $('.securepay_payment_cards .card.amex img').fadeOut(1000);
          }
          else if ($('#edit-creditcard').val().substring(0,2) < 38 && $('#edit-creditcard').val().substring(0,2) > 33) {
            $('.securepay_payment_cards .card.amex img').fadeIn(1000);
            $('.securepay_payment_cards .card.master img').fadeOut(1000);
            $('.securepay_payment_cards .card.visa img').fadeOut(1000);
          }
          else {
            $('.securepay_payment_cards .card.amex img').fadeIn(1000);
            $('.securepay_payment_cards .card.master img').fadeIn(1000);
            $('.securepay_payment_cards .card.visa img').fadeIn(1000);
          }
        });
    });
})(jQuery, Drupal, this, this.document);
