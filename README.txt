SecurePay Payment for Drupal 7.x
================================
This module provides integration with the SecurePay API via the Payment module.


Installation
============
This is a standard and simple module. Place the module in your
sites/all/modules folder, then go to the modules page and enable it.

NOTE: In order to make payments, you will need to enable at least 
one of the sub-modules in addition to the main securepay_payment module.
Some sub-modules require third party registration to work.


Requirements
============
Payment module

SecureXML API sub-module requires the PHP-SecurePay library. You can download this from GitHub at
https://github.com/hash-bang/PHP-SecurePay and unpack it in the
sites/all/libraries folder. Rename the unpacked folder to securepay.
If you want to use FraudGuard, you'll also need to apply the patch
in https://www.drupal.org/node/2298187 to the library.


Suggestions
===========
All payment forms should be secure, so we highly recommend the Securepages module.

You should consider using the Donate Entity module or similar in order to make payments, unless
you are planning to manually implement the payment in your code.


Usage
=====
* Create a new payment type
Go to Configuration > Web services > Payment > Payment methods (/admin/config/services/payment/method) 
and click 'Add payment method'. The options here will depend on which sub-modules you have enabled. 
Each method has different fields with instructional text to guide you.

* Manage existing payment types
Go to Configuration > Web services > Payment > Payment methods (/admin/config/services/payment/method)
From here you can add, edit, view, clone or delete a payment type.


Notes
=====
Please refer to the SecurePay Payment integration guides for more information about a particular method.

The DirectOne sub-module works out of the box, however the Payment entity is NOT updated to 
reflect a successful/declined transaction. This is because it requires a cgi handler on the server in
order to update, therefore this process needs to be handled manually.
Follow examples (such as Direct Post) and the SecurePay integration guide to get an idea of how to do this.

It is best to test these payments out on a public, non-password protected domain name. Most of
these (such as Direct Post) will not work fully on your local environment.


Maintainers
===========
- kafmil (Kurt Foster)
- marc.groth (Marc Groth)
