/**
 * Javascript for SecurePay SecureFrame.
 */

(function ($, Drupal, window, document, undefined) {
    $(document).ready(function(){
        $('.form-submit').click(function(e) {
          $('iframe').addClass('secureframe_iframe');
        });
    });
})(jQuery, Drupal, this, this.document);
