<?php

/**
 * @file
 * Class for SecurePay SecureFrame module.
 */

/**
 * A SecurePay SecureFrame payment method controller.
 */

class SecurePaySecureFrameMethodController extends PaymentMethodController {

  public $controllerDataDefaults = array(
    'bill_name' => 'transact',
    'merchant_id' => 'Please set your Merchant ID',
    'txn_password' => 'Please set your Transaction Password',
    'testmode' => FALSE,
    'fraudguard' => FALSE,
  );

  public $payment_method_configuration_form_elements_callback = 'securepay_secureframe_method_configuration_form_elements';

  /**
   * Construct handler.
   */
  public function __construct() {
    $this->title = t('SecurePay - SecureFrame');
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  public function validate(Payment $payment, PaymentMethod $payment_method, $strict) {}

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    if (isset($_POST['summarycode']) && isset($_POST['rescode']) && isset($_POST['restext'])) {
      $summarycode = $_POST['summarycode'];
      $rescode = $_POST['rescode'];
      $restext = $_POST['restext'];
      // First check for a success.
      if ($summarycode == 1) {
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
        return TRUE;
      }
      else {
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
        drupal_set_message(t('Transaction failed with the error code: !code (!text)', array('!code' => $rescode, '!text' => $restext)), 'error');
        watchdog('securepay_directpost', 'POST Dump: !result', array('!result' => print_r($_POST, 1)));
        throw new PaymentValidationException(t('Transaction failed!'));
      }
    }
  }
}
