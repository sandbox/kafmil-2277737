<?php

/**
 * @file
 * Class for SecurePay DirectOne module.
 */

/**
 * A SecurePay DirectOne payment method controller.
 */

class SecurePayDirectOneMethodController extends PaymentMethodController {

  public $controllerDataDefaults = array(
    'bill_name' => 'transact',
    'vendor_name' => 'Please set your Vendor Name',
    'testmode' => FALSE,
    'fraudguard' => FALSE,
  );

  public $payment_method_configuration_form_elements_callback = 'securepay_directone_method_configuration_form_elements';

  /**
   * Construct handler.
   */
  public function __construct() {
    $this->title = t('SecurePay - DirectOne');
  }
  /**
   * Implements PaymentMethodController::validate().
   */
  public function validate(Payment $payment, PaymentMethod $payment_method, $strict) {}

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {}
}
