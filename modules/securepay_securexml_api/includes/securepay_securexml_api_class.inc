<?php

/**
 * @file
 * Class for SecurePay SecureXML API module.
 */

/**
 * A SecurePay payment method controller.
 */

class SecurePaySecureXMLAPIMethodController extends PaymentMethodController {

  public $controllerDataDefaults = array(
    'merchantid' => 'Please set your Merchant ID',
    'transactionid' => 'Please set your Transaction ID',
    'testmode' => FALSE,
    'fraudguard' => FALSE,
  );

  public $payment_method_configuration_form_elements_callback = 'securepay_securexml_api_method_configuration_form_elements';

  /**
   * Construct handler.
   */
  public function __construct() {
    $this->title = t('SecurePay - SecureXML API');
  }
  /**
   * Implements PaymentMethodController::validate().
   */
  public function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
    if (count($payment->line_items) && $payment->method->controller->name == 'SecurePaySecureXMLAPIMethodController') {
      // Do not process payments of which the description is "foo".
      if (($library = libraries_load('securepay')) && !empty($library['loaded'])) {
        $amount = 0;
        foreach ($payment->line_items as $line_item) {
          $amount += $line_item->amount;
        }
        if ($amount <= 0) {
          throw new PaymentValidationException(t('You cannot process $0 value payments.'));
        }
        // Validate the payment if it's invalid throw an exception.
        $sp = new SecurePay($payment_method->controller_data['merchantid'], $payment_method->controller_data['transactionid']);
        if ($payment_method->controller_data['testmode']) {
          $sp->TestMode();
          if (empty($sp->TestAccountPassword)) {
            $sp->TestAccountPassword = $payment_method->controller_data['transactionid'];
          }
        }
        $sp->Cc = $payment->cc;
        $sp->ExpiryDate = $payment->expdate;
        $sp->ChargeAmount = $amount;
        $sp->ChargeCurrency = $payment->currency_code;
        $sp->Cvv = $payment->cvv;
        $sp->OrderId = $payment->pid;
        // Is the above data valid?
        if ($sp->Valid()) {
          $payment->sp = $sp;
        }
        else {
          drupal_set_message(t('Data validation error: !response', array('!response' => $sp->Error)), 'error');
          throw new PaymentValidationException(t('Validation failed!'));
        }
      }
      else {
        drupal_set_message(t('Please install the PHP-SecurePay library. Check the !readme for further information.', array('!readme' => l(t('read me'), 'admin/help/securepay_securexml_api'))), 'error');
        throw new PaymentValidationException(t('Validation failed!'));
      }
    }
  }
  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    $sp = $payment->sp;
    $response = TRUE;
    if ($payment->method->controller_data['fraudguard']) {
      $fraudguardsp = clone $sp;
      $fraudguardsp->firstName = $payment->firstname;
      $fraudguardsp->lastName = $payment->lastname;
      $fraudguardsp->zipCode = $payment->zipcode;
      $fraudguardsp->billingCountry = $payment->billingcountry;
      $fraudguardsp->deliveryCountry = $payment->deliverycountry;
      $fraudguardsp->ip = ip_address();
      $response = $fraudguardsp->AntiFraudRequest();
    }
    if ($response) {
      $response = $sp->Process();
      if ($response == SECUREPAY_STATUS_APPROVED) {
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
        drupal_set_message(t('Transaction approved'));
        return TRUE;
      }
      else {
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
        drupal_set_message(t('Transaction failed with the error code: !response', array('!response' => $response)), 'error');
        watchdog('securepay_securexml_api', 'XML Dump: !xml', array('!xml' => print_r($sp->ResponseXml, 1)));
        throw new PaymentValidationException(t('Transaction failed!'));
      }
    }
    else {
      $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
      drupal_set_message(t('FraudGuard check failed with the error code: !response', array('!response' => $response)), 'error');
      watchdog('securepay_securexml_api', 'XML Dump: !xml', array('!xml' => print_r($sp->ResponseXml, 1)));
      throw new PaymentValidationException(t('FraudGuard check failed!'));
    }
  }
}
