<?php

/**
 * @file
 * Install, update, and uninstall functions for the SecurePay Payment module.
 */

/**
 * Function securepay_payment_requirements.
 *
 *   Ensure the library is installed during installation.
 *
 * @param string $phase
 *   Either install, undate of runtime.
 *
 * @return array
 *   An array of the requirements for this module.
 */
function securepay_securexml_api_requirements($phase) {
  $requirements = array();
  // Ensure translations do not break at install time.
  $t = get_t();

  $requirements['securepay'] = array(
    'title' => $t('PHP-SecurePay'),
  );

  if (function_exists('libraries_get_libraries')) {
    $libraries = libraries_get_libraries();
    if (isset($libraries['securepay'])) {
      $requirements['securepay']['value'] = $t('Installed');
      $requirements['securepay']['severity'] = REQUIREMENT_OK;
    }
    else {
      if ($phase == 'runtime') {
        $requirements['securepay']['value'] = $t('Not Installed');
      }
      $requirements['securepay']['severity'] = REQUIREMENT_ERROR;
      $requirements['securepay']['description'] = $t('Please install the SecurePay API library. Check the !readme for further information.', array('!readme' => l($t('read me'), 'admin/help/securepay_payment')));
    }
  }

  return $requirements;
}

/**
 * Function securepay_payment_pps_schema.
 *
 *   Create the controller data table.
 *
 * @return array
 *   An array of the requirements for this module.
 */
function securepay_securexml_api_schema() {
  $schema['securepay_securexml_api_payment_method'] = array(
    'fields' => array(
      'pmid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'merchantid' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'transactionid' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'testmode' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'not null' => TRUE,
      ),
      'fraudguard' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'not null' => TRUE,
      ),
      'extras' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data related to this payment method.',
      ),
    ),
    'primary key' => array('pmid'),
    'unique keys' => array(
      'pmid' => array('pmid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function securepay_securexml_api_uninstall() {
  // Delete all relevant payments from the payment_method table.
  db_delete('payment_method')
  ->condition('controller_class_name', 'SecurePaySecureXMLAPIMethodController')
  ->execute();
}

/**
 * Rename the antifraud column to fraudguard.
 */
function securepay_securexml_api_update_7001() {
  db_change_field('securepay_securexml_api_payment_method', 'antifraud', 'fraudguard', array(
    'type' => 'int',
    'size' => 'tiny',
    'default' => 0,
    'not null' => TRUE,
  ));
  drupal_set_message(t('Column antifraud successfully renamed to fraudguard in securepay_securexml_api_payment_method table.'));
}
