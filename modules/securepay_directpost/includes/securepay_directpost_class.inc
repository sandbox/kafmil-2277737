<?php

/**
 * @file
 * Class for SecurePay Direct Post module.
 */

/**
 * A SecurePay Direct Post payment method controller.
 */

class SecurePayDirectPostMethodController extends PaymentMethodController {

  public $controllerDataDefaults = array(
    'eps_merchant' => 'Please set your Merchant ID',
    'eps_txnpassword' => 'Please set your Transaction Password',
    'eps_resulturl' => '',
    'eps_currency' => '',
    'eps_redirect' => FALSE,
    'eps_callbackurl' => '',
    'testmode' => FALSE,
    'eps_txntype' => '',
  );

  public $payment_method_configuration_form_elements_callback = 'securepay_directpost_method_configuration_form_elements';

  /**
   * Construct handler.
   */
  public function __construct() {
    $this->title = t('SecurePay - Direct Post');
  }
  /**
   * Implements PaymentMethodController::validate().
   */
  public function validate(Payment $payment, PaymentMethod $payment_method, $strict) {}

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    // Check the POST data.
    if (isset($_POST['result']) && isset($_POST['result_data'])) {
      $result = $_POST['result'];
      $result_data = $_POST['result_data'];
      // First check for a success.
      if ($result_data == 'SUCCESS') {
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
        return TRUE;
      }
      // Work out if a code was returned.
      else {
        $code_string = substr($result_data, 0, 5);
        if ($code_string == 'CODE:') {
          // The transaction failed. Find out the code and text response to inform the user.
          $code_text = str_replace($code_string, '', $result->data);
          // Split the string to get the relevant data.
          $code_text = explode('|', $code_text);
          $code = $code_text[0];
          $text = $code_text[1];
          $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
          drupal_set_message(t('Transaction failed with the error code: !code (!text)', array('!code' => $code, '!text' => $text)), 'error');
          watchdog('securepay_directpost', 'POST Dump: !result', array('!result' => print_r($result, 1)));
          throw new PaymentValidationException(t('Transaction failed!'));
          // The result data has some unknown value and the transaction definitely failed; inform the user.
        }
        else {
          drupal_set_message(t('Transaction failed with the error: !text', array('!text' => $result_data)), 'error');
          watchdog('securepay_directpost', 'POST Dump: !result', array('!result' => print_r($result, 1)));
          throw new PaymentValidationException(t('Transaction failed!'));
        }
      }
    }
  }
}
